#!/bin/python
import argparse
import numpy as np
import os
import sys
import pathlib
import csv
from Bio import SeqIO

parser = argparse.ArgumentParser(description='''
    Annotating contigs to filter off possible non viral sequences
''')

parser.add_argument('-c', help='VAMB clusterfile')
parser.add_argument('-v', help='Checkv directory')
parser.add_argument('-o', help='File out')

def orf_distribution(sequence_regions,gene_coords,total_genes, viral_genes, host_genes):
    viral_orfs = 0
    host_orfs = 0

    if sequence_regions[0] == 'NA':
        host_orfs = host_genes
        viral_orfs = viral_genes
    else:
        for i, regiontype in enumerate(sequence_regions):
            if regiontype == 'viral':
                start, end = gene_coords[i].split('-')
                viral_orfs += int(end) - int(start)
            elif regiontype == 'host':
                start, end = gene_coords[i].split('-')
                host_orfs += int(end) - int(start)
    viral_percentage = round( (viral_orfs/total_genes)*100,2)
    host_percentage = round( (host_orfs/total_genes)*100, 2)
    NA_percentage = round( 100-viral_percentage-host_percentage,2)
    return (viral_percentage, host_percentage,NA_percentage)

def read_checkv_quality(args):
    '''
    1. Load annotation of each Bin from the checkV quality_summary file 
    2. Define Pro-viral Regions 
    '''

    quality_file = os.path.join(args.v,'quality_summary.tsv')
    viruses = dict()
    with open(quality_file,'r') as infile:
        reader = csv.DictReader(infile,delimiter='\t') 
        for row in reader:
            contigid  = row['contig_id']
            if not row['checkv_quality'] in ['Medium-quality','High-quality','Complete']:
                continue
            viruses[contigid] = row

    vamb_cluster_file = os.path.join(args.c) 

    with open(args.o, 'w', newline='') as csvfile:
        header = ['vamb_bin','contig_id','contig_length','provirus',
        'proviral_length','gene_count','viral_genes',
        'host_genes','checkv_quality','miuvig_quality','completeness',
        'completeness_method','contamination','kmer_freq','warnings']
        writer = csv.DictWriter(csvfile, fieldnames=header,delimiter='\t')
        writer.writeheader()
        with open(vamb_cluster_file,'r') as infile:
            for line in infile:
                binid, contigid = line.strip().split()
                if contigid in viruses:
                    viruses[contigid]['vamb_bin'] = binid
                    writer.writerow(viruses[contigid])



            

    return viruses


if __name__ == "__main__":
    '''

    '''
    args = parser.parse_args()
    viruses = read_checkv_quality(args)
