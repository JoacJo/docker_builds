#1/bin/python
import numpy as np
import argparse
import gzip
import time


class genome():
    def __init__(self,genome_id,genome_len):
        self.genome_id = genome_id
        self.genome_len = float(genome_len)

def parse_fai(fai_file):
    genomes = dict()
    with open(fai_file,'r') as infile:
        for line in infile:
            genomeid, len, g1, g2, g3 = line.strip().split()
            g = genome(genomeid,len)
            genomes[genomeid] = g
    return genomes


def yield_alignment_blocks(handle):
        # init block with 1st record
        key, alns = None, None
        for aln in parse_blast(handle):
                key = aln['tname']
                alns = [aln]
                break
        # loop over remaining records
        for aln in parse_blast(handle):

                # extend block
                if aln['tname'] == key:
                        alns.append(aln)
                # yield block and start new one
                else:
                        yield alns
                        key = aln['tname']
                        alns = [aln]
        yield alns

def parse_blast(handle):
        for line in handle:
                r = line.split()
                yield {
                        'qname':r[0],
                        'tname':r[1],
                        'pid':float(r[2]),
                        'len':float(r[3]),
                        'qcoords':sorted([int(r[6]), int(r[7])]),
                        'tcoords':sorted([int(r[8]), int(r[9])]),
                        'evalue':float(r[-1])
                        }

def compute_ani_cov(alns,genomes):
    genomeid = alns[0]['tname']
    g = genomes[genomeid]
    genome_len = g.genome_len
    
    ### Genome coverage
    coords = sorted([a['tcoords'] for a in alns])
    
    nr_coords = [coords[0]]
    for start, stop in coords[1:]:
            # overlapping, update start coord
            if start <= (nr_coords[-1][1] + 1):
                nr_coords[-1][1] = max(nr_coords[-1][1], stop)
            # non-overlapping, append to list
            else:
                nr_coords.append([start, stop])
    percentage_covered =  round((sum([stop - start + 1 for start, stop in nr_coords])/genome_len)*100,2)
    
    ### ANI
    ani = round(sum(a['len'] * a['pid'] for a in alns)/sum(a['len'] for a in alns),2)
    hits = len(alns)
    row = [genomeid, genome_len, hits, ani, percentage_covered ]
    return row


def prune_alns(alns, min_length=0, min_evalue=1e-3):
        # remove short aligns
        alns = [aln for aln in alns if aln['len'] >= min_length and aln['evalue'] <= min_evalue]
        return alns


def parse_arguments():
        parser = argparse.ArgumentParser()
        parser.add_argument('-i', dest='input', type=str, required=True, metavar='PATH',
                help="path to blastn input file (format: 'std 6 ')")
        parser.add_argument('-f', dest='faifile', type=str, required=True, metavar='PATH',
                help="path to Fasta index for genomes")       
        parser.add_argument('-o', dest='output', type=str, required=True, metavar='PATH',
                help="path to ani file")
        parser.add_argument('-l', dest='length', type=int, metavar='INT',
                help="minimum alignment length to keep")
        return vars(parser.parse_args())
                
if __name__ == "__main__":
        args = parse_arguments()
        genomes = parse_fai(args['faifile'])

        out = gzip.open(args['output'], 'w') if args['output'].split('.')[-1]=='gz' else open(args['output'], 'w')
        fields = ['genome','size', 'num_alns', 'ani', 'genome_coverage']
        out.write('\t'.join(fields)+'\n')
        input = gzip.open(args['input']) if args['input'].split('.')[-1]=='gz' else open(args['input'])
        
        j = 0
        start_time = time.time()
        for alns in yield_alignment_blocks(input):
            if j % 10000 == 0:
                print('Parsed ',str(j),' Genomes')
            j += 1
            row = compute_ani_cov(alns, genomes)
            row = [str(k) for k in row]
            out.write('\t'.join(row)+'\n')
        out.close()

